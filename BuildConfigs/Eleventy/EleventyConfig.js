
const Node_FS = require('fs');
const Node_Path = require('path');

const Chalk = require("chalk");
const Eleventy_RSSPlugin = require('@11ty/eleventy-plugin-rss');



//
//
// Properties
// ================================================================================================
// ================================================================================================

const PrintEnvLoads_BOOL = false;

let HTML_TemplatesInput_FQ_PATH = "";
let HTML_TemplatesInput_Relative_PATH = "";
let Output_Relative_PATH = "";

let Filters_FQ_PATH = "";




//
//
// Setup Eleventy (Build) Data
// ================================================================================================
// ================================================================================================

const EleventyProcessInfo = {
	PID: process.pid,
	Platform: process.platform
};
console.log(`EleventyProcessInfo... %o`, EleventyProcessInfo);

const ProjectRootRelative_PATH = "../../";
const ProjectRoot_PATH = Node_Path.resolve(__dirname, ProjectRootRelative_PATH);
const EnvVarsBaseFileName_PATH = `./BuildConfigs/build_settings`;
const ProjectProperties_PATH = Node_Path.join(ProjectRoot_PATH, `./BuildConfigs/ProjectProperties.js`);

const BuildEnvVars = require('../LoadBuildEnv.js')("EleventyConfig", PrintEnvLoads_BOOL);
console.log(`Build Environment Settings Load Complete...`);

const ServerEnvVars = require('../../ServerConfigs/LoadServerEnv.js')("EleventyConfig", PrintEnvLoads_BOOL);
console.log(`Server Environment Settings Load Complete...`);

const ProjectProperties = require(ProjectProperties_PATH);
console.log("Project Properties Load Complete...");

// Set Path Properties
HTML_TemplatesInput_FQ_PATH = Node_Path.resolve(ProjectRoot_PATH, ProjectProperties.src.html_built_templates);
HTML_TemplatesInput_Relative_PATH = ProjectProperties.src.html_built_templates;
Output_Relative_PATH = ProjectProperties.temp.html_built_templates;
Filters_FQ_PATH = Node_Path.resolve(ProjectRoot_PATH, ProjectProperties.src.html_built_templates + "/_filters");

if (PrintEnvLoads_BOOL) {
	console.log("Loaded Build ENV Settings: %o", BuildEnvVars);
	console.log(`---------------------------------------------------------`);
	console.log(`Full process env...`);
	console.log("process.env %o", process.env);
	console.log(`---------------------------------------------------------`);
	console.log(`--> Eleventy PATHs  -  ProjectRoot_PATH : %s`, ProjectRoot_PATH);
	console.log(`--> Eleventy PATHs  -  HTML_TemplatesInput_FQ_PATH : %s`, HTML_TemplatesInput_FQ_PATH);
	console.log(`--> Eleventy PATHs  -  HTML_TemplatesInput_Relative_PATH : %s`, HTML_TemplatesInput_Relative_PATH);
	console.log(`--> Eleventy PATHs  -  Output_Relative_PATH : %s`, Output_Relative_PATH);
	console.log(`--> Eleventy PATHs  -  Filters_FQ_PATH : %s`, Filters_FQ_PATH);
};

const BuildData = {
	BuildEnv: BuildEnvVars,
	ServerEnv: ServerEnvVars,
	ProjectProperties: ProjectProperties
};



//
//
// Exported EleventyConfig
// ================================================================================================
// ================================================================================================

module.exports = function (eleventyConfig)
{
	//
	//
	// Main Configuration Properties
	// ===========================================================================
	// ===========================================================================

	const BaseConfigs = {
		dir: {
			input: HTML_TemplatesInput_Relative_PATH,
			output: Output_Relative_PATH,

			includes: "_11ty_includes", // Relative to input base
			layouts: "_11ty_layouts", // Relative to input base
			data: "_global_template_data"

		},
		templateFormats: [
			"html", "md", // Assets Template Processed
			"11ty.js", "ejs", "haml", "hbs", "liquid", "mustache", "njk", "pug", // True Template Formats
			// Raw Assets can be added (and thereby be copied though to output) by including their extensions here.
			// But, doing it this way, the only files that will be handled (i.e. copied though to output) are those
			// that are explicitly referenced in processed templates (via frontmatter etc.).
			//"gif", "jpg", "jpeg", "png", "svg"
		],
		htmlTemplateEngine: "liquid",
		markdownTemplateEngine: "liquid",
		pathPrefix: "", // Prefix added to processed URLS
		htmlOutputSuffix: "-o", // Suffix added to output file names,
		// Suffix is ONLY used when files are set to use the same input and output dir
		jsDataFileSuffix: ".11tydata", // // Suffix added to data files
	};

	// Expose Build Data for Template Use
	eleventyConfig.addGlobalData("BuildData", BuildData);


	//
	//
	// Passthrough Copies
	// ===========================================================================
	// ===========================================================================

	// Copy files wholesale (no processing). Note: Paths for addPassthroughCopy() are handled from your project
	// root (not from your input path). That said, if the input path is in the path passed to addPassthroughCopy()
	// it is removed and replaced by the output path to determine output location.

	// Copy all image formats, retaining their relative paths to content.
	eleventyConfig.addPassthroughCopy(HTML_TemplatesInput_Relative_PATH + "/**/*.gif");
	eleventyConfig.addPassthroughCopy(HTML_TemplatesInput_Relative_PATH + "/**/*.jpg");
	eleventyConfig.addPassthroughCopy(HTML_TemplatesInput_Relative_PATH + "/**/*.jpeg");
	eleventyConfig.addPassthroughCopy(HTML_TemplatesInput_Relative_PATH + "/**/*.png");
	eleventyConfig.addPassthroughCopy(HTML_TemplatesInput_Relative_PATH + "/**/*.svg");



	//
	//
	// Transforms - Apply to All Templates During Processing
	// ===========================================================================
	// ===========================================================================




	//
	//
	// Filters - Applied to Elements that "Tagged" with the Filter
	// (e.g. {{ myPathVariableElement | url_encode }})
	// ===========================================================================
	// ===========================================================================



	// Externally Sourced Filers
	// --------------------------------------------------



	//
	//
	// Collections - Predefined Collection Generation
	// ===========================================================================
	// ===========================================================================

	// Note: Content templates can dynamically generate "Tag-based" collections by simply defining tags they
	// apply to. Each used tag will create or add to that tag's collection.
	//
	// This addCollection method requires that the targeted template by otherwise ingested. That is, collections
	// won't be created just based on a file path for un-processed templates or files.



	//
	//
	// Plugins
	// ===========================================================================
	// ===========================================================================

	eleventyConfig.addPlugin(Eleventy_RSSPlugin);


	// Return the Base Configs
	return BaseConfigs;
};
