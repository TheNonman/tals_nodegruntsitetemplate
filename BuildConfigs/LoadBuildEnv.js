
const Node_FS = require('fs');
const Node_Path = require('path');

const Chalk = require("chalk");



module.exports = function (opt_ProcessName, debug)
{
	//
	//
	// Properties
	// ================================================================================================
	// ================================================================================================

	const PrintLoadDebug_BOOL = debug ? debug : false;

	if (PrintLoadDebug_BOOL) {
		console.log(`LoadBuildEnv ::`);
		console.log(`............ :: opt_ProcessName : ${ opt_ProcessName }`);
		console.log(`............ :: debug : ${ debug }`);
	}

	const ProjectRootRelative_PATH = "../";
	const ProjectRoot_PATH = Node_Path.resolve(__dirname, ProjectRootRelative_PATH);
	const EnvVarsBaseFileName_PATH = `./BuildConfigs/build_settings`;

	const FinalEnvVars = LoadEnvFile(EnvVarsBaseFileName_PATH);

	if (PrintLoadDebug_BOOL) {
		if (opt_ProcessName) {
			console.log(`${ opt_ProcessName } :: Build Environment Settings Load Complete...`);
		}
		else {
			console.log(`Build Environment Settings Load Complete (un-named process)...`);
		}
		console.log("Loaded Build FinalEnvVars ");
		console.log(`---------------------------------------------------------`);
		console.log("%o", FinalEnvVars);
		console.log(`---------------------------------------------------------`);
		console.log(`---------------------------------------------------------`);
		console.log("Process Env - After LoadBuildEnv ");
		console.log(`---------------------------------------------------------`);
		console.log("%o", process.env);
	};

	function LoadEnvFile ( baseFileNamePath )
	{
		let defaultEnv = {};
		let customEnv = {};

		const CustomSettingsEnv_Path = Node_Path.resolve(ProjectRoot_PATH, `${baseFileNamePath}.env`);
		const DefaultSettingsEnv_Path = Node_Path.resolve(ProjectRoot_PATH, `${baseFileNamePath}_default.env`);

		if (PrintLoadDebug_BOOL) {
			console.log(`LoadBuildEnv :: FilePaths...`);
			console.log(`............ :: CustomSettingsEnv_Path : ${ CustomSettingsEnv_Path }`);
			console.log(`............ :: DefaultSettingsEnv_Path : ${ DefaultSettingsEnv_Path }`);
		}

		// Process Environment Variables (i.e. process.env) are immutable once added (constants)
		// So, add local custom env values first, then enhance them with any missing defaults.
		if (Node_FS.existsSync(CustomSettingsEnv_Path)) {
			customEnv = require('dotenv').config({ path: CustomSettingsEnv_Path }).parsed;

			if (PrintLoadDebug_BOOL)		console.log("Build customEnv ENV: %o", customEnv);
		}
		else {
			console.log(Chalk.yellow(`NOTE :`) + `  No Custom Build ENV settings file ('.env') loaded - "${baseFileNamePath}.env" !!!`);
			console.log(`... To customize Build settings, duplicate the defaults File ('${baseFileNamePath}_default.env') and rename it: ${baseFileNamePath}.env`);
		}

		// Now enhance the environment variables with the default values for any missing.
		if (Node_FS.existsSync(DefaultSettingsEnv_Path)) {
			defaultEnv = require('dotenv').config({ path: DefaultSettingsEnv_Path }).parsed;

			if (PrintLoadDebug_BOOL)		console.log("Build DEFAULT ENV: %o", defaultEnv);
		}
		else {
			console.log(Chalk.red(`ERROR :  No valid Build Default ENV settings file ('.env') found - "${ DefaultSettingsEnv_Path }" !!!`));
			throw `ERROR: Build Default ENV settings file ('.env') failed loading - ${baseFileNamePath}_default.env !!!`;
		}

		// Generate a combined set of vars. Here, defaults are first as same named keys will be overwritten
		// using the spread merge method.
		const combinedEnv = {
			...defaultEnv,
			...customEnv
		}

		return combinedEnv;
	};

	return FinalEnvVars;
}
