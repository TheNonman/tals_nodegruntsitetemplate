
module.exports =
{
	Production: {
		debug: false,
		node: true,
		browser: true,
		jquery: true, // Support for jQuery $ Def
		esnext: true,
		bitwise: true,
		curly: false,
		eqeqeq: false, // Suppress Warnings About Using == instead of ===
		immed: true,
		latedef: 'nofunc', // Suppress Warnings About Functions being used before declaration
		newcap: false,
		noarg: true,
		regexp: true,
		undef: true,
		unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
		strict: true,
		trailing: false, // Allow Trailing Whitespace
		smarttabs: true,
		laxbreak: true,
		laxcomma: true, // Allow Comma-first Object Definition Syntax
		white: false // Don't Warn About Whitepace/tab Counts
	},
	Debug : {
		debug: true,
		node: true,
		browser: true,
		jquery: true, // Support for jQuery $ Def
		esnext: true,
		bitwise: true,
		curly: false,
		eqeqeq: false,  // Suppress Warnings About Using == instead of ===
		immed: true,
		latedef: 'nofunc', // Suppress Warnings About Functions being used before declaration
		newcap: false,
		noarg: true,
		regexp: true,
		undef: true,
		unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
		strict: true,
		trailing: false, // Allow Trailing Whitespace
		smarttabs: true,
		laxbreak: true,
		laxcomma: true, // Allow Comma-first Object Definition Syntax
		white: false // Don't Warn About Whitepace/tab Counts
	}
};
