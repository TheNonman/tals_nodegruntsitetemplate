module.exports = function (grunt, options)
{
	return {
		options: {
			sourceMap: false,
			presets: ['env', 'react']
		},
		dist: {
			files: {
				// Output_File_Path : Input_Entry_File_Path - Sample Only Source Not Included
				'<%= BuildConfigs.temp.js %>/react_unpackaged.js': '<%= BuildConfigs.src.js %>/react_unpackaged.js',
				'<%= BuildConfigs.temp.js %>/preact_unpackaged.js': '<%= BuildConfigs.src.js %>/preact_unpackaged.js'
			}
		}
	}
};
