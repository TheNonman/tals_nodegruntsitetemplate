module.exports = function (grunt, options)
{
	return {
		image_files_ToTemp: {
			files: [{
				expand: true,
				cwd: '<%= BuildConfigs.src.images %>',
				src: ['**/*.jpg', '**/*.png', '**/*.gif', '**/*.svg'],
				dest: '<%= BuildConfigs.temp.images %>'
			}]
		},
		image_files_ToStaging: {
			files: [{
				expand: true,
				cwd: '<%= BuildConfigs.temp.images %>',
				src: ['**/*.jpg', '**/*.png', '**/*.gif', '**/*.svg'],
				dest: '<%= BuildConfigs.staging.images %>'
			}]
		}
	}
};
