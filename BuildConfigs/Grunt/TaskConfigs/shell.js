
module.exports = function (grunt, options)
{
	return {
			options: {
				stdout: true,
				stderr: true,
				stdin: true,
				failOnError: true,
				stdinRawMode: false
			},
			eleventy: {
				command: 'npx @11ty/eleventy --config=BuildConfigs/Eleventy/EleventyConfig.js'
			}
	}
};
