module.exports = function (grunt, options)
{
	return {
		icons: {
			src: '<%= BuildConfigs.src.fonts_to_build %>/**/*.svg',
			dest: '<%= BuildConfigs.temp.fonts_built %>',
			destScss: '<%= BuildConfigs.src.sass_generated %>',
			options: {
				font: 'testFont',
				fontFamilyName: "testFont", // Sting Name of Font, Used for CSS Font Family and Font Selection in Apps When Installed
				fontFilename: "testFont", // String Font file names
				hashes: true,
				styles: 'font,icon',
				types: 'woff2,woff,ttf,svg',
				order: 'woff2,woff,ttf,svg',
				syntax: 'bem',
				template: '',
				templateOptions: {},

				stylesheets: ['scss'],
				relativeFontPath: '/fonts',
				fontPathVariables: false,
				version: '1.0.0',

				engine: 'fontforge', // Rendering Engine used for Generation of Font Files | Options: fontforge, node
				autoHint: false, // 2022/07 TTFAutoHint is Crashing FontForge

				// Save and Reuse the Same Codepoints for each glyph
				codepointsFile: '<%= BuildConfigs.src.fonts_to_build %>/glyph_codepoints.json',
				descent: 64, // AKA Baseline
			},
		}
	}
};
