
const Node_FS = require('fs');
const Node_Path = require('path');

module.exports = function (grunt, options)
{
	const DebugPrints_BOOL = false;

	let serverProxyValue = null;
	let serverStaticValue = null;

	let proxyTargetPortSetting = "";
	let proxyTargetPort = "";
	let proxyTargetURL = "";

	if(DebugPrints_BOOL){
		console.log(`Config Browser Sync Task`);
		console.log(`--> Options...`);
		console.log(`%o`, options);
	}

	const ValidProxyTargets = [
		"EXPRESS",
		"KOA",
		"LOCAL"
	];

	const browserSyncServerPort = parseInt(options.ServerEnvSettings.BROWSER_SYNC_PROXY_PORT) || 9000;
	const browserSyncUIPort = parseInt(options.ServerEnvSettings.BROWSER_SYNC_UI_PORT) || 9001;

	const proxyTarget = ValidateProxyTargets(options.ServerEnvSettings.BROWSER_SYNC_SERVER_PROXY_TARGET.toUpperCase());
	if (proxyTarget === "LOCAL") {
		if (!options.ServerEnvSettings.BROWSER_SYNC_SERVER_CUSTOM_STATIC_FILE_DIR
			| options.ServerEnvSettings.BROWSER_SYNC_SERVER_CUSTOM_STATIC_FILE_DIR.length == 0)
		{
			console.log (`BrowserSync ERROR : To run a local static files server, a path must be provided in "BROWSER_SYNC_SERVER_CUSTOM_STATIC_FILE_DIR" !!!`);
			throw `Invalid BrowserSync Static Files Path !`;
		}
		const staticFilesPath = Node_Path.resolve(options.ServerEnvSettings.BROWSER_SYNC_SERVER_CUSTOM_STATIC_FILE_DIR);
		if (!Node_FS.existsSync(staticFilesPath)) {
			console.log (`BrowserSync ERROR : Cannot resolve the static file path: ${options.ServerEnvSettings.BROWSER_SYNC_SERVER_CUSTOM_STATIC_FILE_DIR}`);
			console.log (`--> Please verify the directory exists !`);
			throw `Invalid BrowserSync Static Files Path: "${staticFilesPath}"`;
		}

		serverStaticValue = {
			baseDir: staticFilesPath,
			index: "index.html",
			directory: false, // Allow Directory Navigation in Browsers
			serveStaticOptions: {
				extensions: ["html", "htm"]
			}

		}
	}
	else {
		proxyTargetPortSetting = `${proxyTarget}_SERVER_PORT`;
		proxyTargetPort = options.ServerEnvSettings[proxyTargetPortSetting] || "8080";
		proxyTargetURL = "localhost:" + proxyTargetPort;
		serverProxyValue = proxyTargetURL;
	}

	if(DebugPrints_BOOL){
		console.log ("BrowserSync Ports to Use");
		console.log (`--> BrowserSync Server Port : ${browserSyncServerPort}`);
		console.log (`--> BrowserSync UI Port : ${browserSyncUIPort}`);
		if (proxyTarget != "LOCAL") {
			console.log (`BrowserSync will Proxy the ${proxyTarget} Server`);
			console.log (`--> Server Proxy Target : ${proxyTarget}`);
			console.log (`--> Server Proxy Target Port Setting : ${proxyTargetPortSetting}`);
			console.log (`--> Proxy URL : ${proxyTargetURL}`);
		}
		else {
			console.log (`BrowserSync will Run a Local Static File Server...`);
			console.log (`--> Static Files will be served from : ${serverStaticValue.baseDir}`);
		}
	}




	function ValidateProxyTargets ( theTarget ) {
		if (ValidProxyTargets.indexOf(theTarget) === -1) {

			console.log (`BrowserSync ERROR : ${theTarget} is Not a Valid BrowserSync Proxy Target.`);
			throw `Invalid BrowserSync Proxy Target: "${theTarget}"`;
		}

		return theTarget;
	}

	return {
		options: {
			// Basic Settings
			proxy: serverProxyValue,
			server: serverStaticValue,
			port: browserSyncServerPort,
			cors: false, // Add HTTP access control (CORS) headers to assets served by BrowserSync?
			watchTask: true,
			ui: {
				port: browserSyncUIPort
			},

			// BrowserSync Startup Settings
			browser: ['chrome', 'firefox', 'safari'], // Browser(s) to Open
			open: 'local',
			startPath: null, // Path (from domain root) to use on open - Default: null
			scriptPath: undefined, // Function for changing the injected BrowserSync Javascript path

			// Logging Settings
			logLevel: 'info', // Options: info (default), debug, warn, or silent
			logPrefix: '[BrowserSync]', // Prefix for all log messages from BrowserSync
			logConnections: true, // Options: true or false
			logFileChanges: true, // Options: true or false
			logSnippet: true, // Options: true or false - Only for Snippet Mode

			// User Event Propagation Settings
			ghostMode: { // Propagate the enabled events across active browser sessions
				clicks: true,
				forms: true,
				scroll: true
			},
			scrollProportionally: true, // Options: true (default) or false
			scrollThrottle: 100, // Event Propagation Throttle in MS (default 0)
			scrollRestoreTechnique: 'window.name', // Options: 'window.name' (default) or 'Cookie'
			scrollElements: [], // Add CSS selectors to Support Scroll Updating On
			scrollElementMapping: [], // Add selectors that should have synced scrollTop positions

			// Page Event Settings
			codeSync: true, // Send File Change Events?
			injectChanges: true, // Inject detected changes to asset files (CSS and images)
			reloadDelay: 0, // MS to wait before invoking Reload/Inject after a file change event
			reloadDebounce: 750, // MS of event silence required before a reload invocation
			reloadOnRestart: false, // Invoke a Reload when BrowserSync restarts?
			reloadThrottle: 0, // MS during which additional Reload events will be skipped
			timestamps: false, // Append timestamps to injected files?

			// BrowserSync Plugins
			plugins: []
		},
		dev_mac: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['google chrome']
			}
		},
		dev_mac_all: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['google chrome', 'firefox', 'safari']
			}
		},
		dev_nix: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome']
			}
		},
		dev_nix_all: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome', 'firefox']
			}
		},
		dev_win: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome']
			}
		},
		dev_win_all: {
			bsFiles: {
				src: '<%= BuildConfigs.staging.root %>/**/*'
			},
			options: {
				browser: ['chrome', 'firefox']
			}
		}
	}
};
