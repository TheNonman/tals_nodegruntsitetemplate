module.exports = function (grunt, options)
{
	return {
		dev: {
			options: {
				script: 'ServerConfigs/ExpressServer/index.js'
			}
		},
		prod: {
			options: {
				script: 'ServerConfigs/ExpressServer/index.js',
				node_env: 'production'
			}
		},
		test: {
			options: {
				script: 'ServerConfigs/ExpressServer/index.js'
			}
		}
	}
};
