module.exports = function (grunt, options)
{
	return {
		options: {},
		sass: {
			files: ['<%= BuildConfigs.src.sass_main %>/**/*.scss'],
			tasks: ['sass:production_site', 'sync:css_ToStaging']
		},
		sass3p: {
			files: ['<%= BuildConfigs.src.sass_3P %>/**/*.scss'],
			tasks: ['sass:production_3P', 'sync:css_ToStaging']
		},
		css: {
			files: ['<%= BuildConfigs.src.css_unprocessed %>/**/*.css'],
			tasks: ['sync:css_ToTemp', 'sync:css_ToStaging']
		},
		html: {
			files: ['<%= BuildConfigs.src.html %>/**/*.html'],
			tasks: ['sync:html_ToStaging']
		},
		html_built_templates: {
			files: ['<%= BuildConfigs.src.html_built_templates %>/**/*'],
			tasks: ['shell:eleventy', 'sync:html_BuiltTemplates_ToStaging']
		},
		js: {
			files: [	'<%= BuildConfigs.src.js %>/**/*.js',
						'!<%= BuildConfigs.src.js_3P %>/**/*.js'
			],
			tasks: [	'jshint:sourceDebug',
						'sync:js_ToTemp',
						'sync:js_ToStaging'
			]
		}
	}
};
