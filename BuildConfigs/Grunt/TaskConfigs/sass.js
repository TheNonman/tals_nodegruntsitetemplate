
const sass = require('sass');

module.exports = function (grunt, options)
{
	return {
		options: {
			implementation: sass,
			includePaths: ['<%= BuildConfigs.src.css %>'],
			sourceMap: true
		},
		production_site: {
			files: [
				{
					'<%= BuildConfigs.temp.css %>/<%= BuildConfigs.finalFileNames.css_Screen %>' :
					'<%= BuildConfigs.src.sass_main %>/screen.scss'
				},
			]
		},
		production_3P: {
			files: [
				{
					'<%= BuildConfigs.temp.css %>/someSeparate3P.css' :
					'<%= BuildConfigs.src.sass_3P %>/someSeparate3P.scss'
				},
			]
		}
	}
};
