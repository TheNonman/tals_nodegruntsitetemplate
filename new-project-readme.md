
# Introduction

TODO: Give a short introduction of your project. Let this section explain the objectives, motivation, summary of this project. 

# Prerequisites / Dependencies

This project is based on [Tal's Node Grunt Site Template](https://bitbucket.org/TheNonman/tals_nodegruntsitetemplate) and all of the same / usual requirements apply (for info on the features and support provided by the template, refer to its ReadMe in the Documentation folder [here](./Documentation/zzz_TalsNodeGruntSiteTemplate_ReadMe.md)).

1. [NodeJS](https://nodejs.org) --  (latest tested version 16.13.1)

1. NodeJS NPM (installed with NodeJS)

1. The Grunt-CLI installed Globally  (`npm install -g grunt-cli`)

1. *Optional*: The Karma-CLI installed Globally  (`npm install -g karma-cli`). There is prep (grunt tasking) support for testing using Karma, but there are no tests setup for anything in the template itself. This keeps the Karma-CLI optional.

1. *Optional*: FontForge. *Disabled by Default.* Grunt-Webfont (AKA "SVG to webfont converter for Grunt") can run under either of two font rendering engines. Its default is to use a local install of FontForge (commandline). Its Node (only) engine option requires no additional system dependencies, but the author says it has problems with some SVG files and it has no support for ligatures. If you are going to want the better feature support via FontForge, you will need to install FontForge separetely. See "Installing FontForge and/or TTFAutoHint" below if needed.

1. *Optional*: ttfautohint. *Disabled by Default.* For proper hinting by Grunt-Webfont. See "Installing FontForge and/or TTFAutoHint" below if needed.

1. *Optional*: OpenFL. *Disabled by Default.* The template's gruntfile has support for including an OpenFL compiling step in your build process. To enable this, see the "Enabling OpenFL" section below.

# Getting Started

Assuming that you have the prerequisites installed on your environment...

1. Sync this Git repo

2. Open a Command Line / Terminal targeting the local repo directory.

  1. Execute `npm install`
  3. Execute: `grunt Work`

Grunt will build a Staging site from the content in AppSource, launch that Staging site via an Express server, bind it up with BrowserSync and open the site in Chrome (assuming you have it installed) with dev file watches set for changes.

When ready, use Cntrl-C to shut down the server and watches.

# Grunt Task Help

Execute `grunt Help` or just `grunt` to get a list of available tasks, with those tasks intended to be called directly by users specifically called out.


# Additional Documentation

Additional info and documentation can be found in the repo's 'Documentation' directory.


# Markdown and ReadMe References

If you want to learn more about editing this ReadMe and creating good markdown-based readme files, refer the following sources:

* [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

* [Markdown-Guide](http://markdown-guide.readthedocs.io/en/latest/basics.html)

* [Visual Studio ReadMe Guidelines](https://www.visualstudio.com/en-us/docs/git/create-a-readme)


