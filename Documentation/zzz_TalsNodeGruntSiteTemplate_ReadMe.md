# Introduction

A Template project that can be used to quickly scaffold a new NodeJS-Grunt website project with an Express web server pre-configured to run via localhost:3300 and a BrowserSync server setup to proxy the express serrver using the BrowserSync default ports (3000 for the main http server, 3001 for the BrowserSync UI).

Projects scaffolded with 'Tal's Node Grunt Site Template' are ready to go with a full "Source -> Temp -> Staging" build flow to fully support complex pre-compiler and optimization build scenarios.

In addition, BacktopJS is included (and pre-configured) for visual regression testing.

## Included Feature Support

 - Compiling Support:
	- EJS HTML Template Pre/Build-Time Compiling
	- Untemplated HTML
	- JSHint
	- JSDoc
	- SASS/SCSS (via Compass)
	- Custom Icon Web Font Building from SVG Source (disabled by default)
	- OpenFL Compiling Support (disabled by default)

 - Workflow Support:
	- Pre-configured Express Server
	- BrowserSync (Proxying Express Server)
	- BackstopJS Visual Regression Testing



# Prerequisites / Dependencies

Use of this template assumes the following installed on the target computer/environment (Note: Ruby and Compass for are required for SASS/SCSS support):

1. [NodeJS](https://nodejs.org)

1. NodeJS NPM (installed with NodeJS)

1. [Ruby](https://www.ruby-lang.org)

1. The Ruby Compass Gem  (`gem install compass`)

1. The Grunt-CLI installed Globally  (`npm install -g grunt-cli`)

1. *Optional*: The Karma-CLI installed Globally  (`npm install -g karma-cli`). There is prep (grunt tasking) support for testing using Karma, but there are no tests setup for anything in the template itself. This keeps the Karma-CLI optional.

1. *Optional*: FontForge. *Disabled by Default.* Grunt-Webfont (AKA "SVG to webfont converter for Grunt") can run under either of two font rendering engines. Its default is to use a local install of FontForge (commandline). Its Node (only) engine option requires no additional system dependencies, but the author says it has problems with some SVG files and it has no support for ligatures. If you are going to want the better feature support via FontForge, you will need to install FontForge separetely. See "Installing FontForge and/or TTFAutoHint" below if needed.

1. *Optional*: ttfautohint. *Disabled by Default.* For proper hinting by Grunt-Webfont. See "Installing FontForge and/or TTFAutoHint" below if needed.

1. *Optional*: OpenFL. *Disabled by Default.* The template's gruntfile has support for including an OpenFL compiling step in your build process. To enable this, see the "Enabling OpenFL" section below.

# Getting Started

1. Create an empty directory for a new project (No NPM package.json, See First Steps below).

1. Open a Command Line / Terminal targeting the empty local project directory created.

1. Execute: `npm install git+https://TheNonman@bitbucket.org/TheNonman/tals_nodegruntsitetemplate --no-save`
    
    - Note: the '--no-save' flag is important to make sure that the templating project itself is not stored as a dependancy of the new project.

1. Execute: `grunt Work`

Grunt will build a Staging site from the content in AppSource, launch that Staging site via an Express server, bind it up with BrowserSync and open the site in Chrome (assuming you have it installed) with dev file watches set for changes. 

When ready, in your Command Line / Terminal, hit Control-C to shut down the server and watches.


## First Steps

Edit the generated NPM package.json with info about your project. As an aside, don't run `npm init` or create an NPM package.json file in the project directory before installing the scaffold. The scaffolding install process will add a package.json for you, ready for editing and later reinstalling, updating, etc.

Edit the generated ReadMe.md with info about your project.

Open the generated Documentation folder, edit the starting 01\_About.md with info about your project and fill in more documentation details as you go. If you include JSDoc comments in your JavaScript source files, you can execute `grunt Docs` to have JSDoc HTML documentation files added in "Documentation -> API\_Docs"

Don't forget to save your scaffolded project off to version control. 

Edit, create, enjoy.


## Build and Test

As noted above, the Work grunt task (executed with `grunt Work`) will build a fully navigatable and testable Staging environment using a complete "source to temp to staging" build flow. It will then spin up a NodeJS Express web server bound to the produced staging environment ready to be accessed.

The Express web server is set by default to using port 3300. This can be changed by simply editing the Express server configuration in 'ServerConfigs/server_app.js'.

Of course, if you change the main server port, you will want to also change the server URL that BrowserSync is set to proxy appropriately. This can be changed in the gruntfile. Just change it in the 'options' object of the 'browserSync' task definition.

The main working task, `grunt Work`, will also setup watches for changes on your source files, open Chrome (if available), and load the staged site/content for testing as you work. This single browser test environment is great for working quickly. If/when you're ready to do cross-browser testing, you can use `grunt MultiWork`. This task works the same as the main Work task, except that it will open, load, and update your staged project across a series of web browsers simultaneously. The browsers included are based on your working environment platform. On Mac, it tries to include (based on availability) Chrome, Firefox, and Safari. On Linux, it will try to include Chrome and Firefox. On Windows, it will try to include Chrome, Firefox, Internet Explorer and MS Edge.

See the section on Grunt Task Help below to look into other available tasks.


## BackstopJS Visual Regression Testing

After you have run `grunt Work` or `grunt BasicBuild` (builds with no server startup or watches) at least once, you can use `grunt Tests` to execute image-based, visual regression tests via BackstopJS.

The first-time this is run, BackstopJS will indicate a regression failure. This is simply because no baseline reference images are included from the template repo (i.e. you should see that there are no images in the reference column of the displayed report). Execute `grunt TestsApproved` to promote the current test images as the approved reference images and from there, tests should only fail if there are visual changes.

Git Support Note: By default, the template's included .gitignore has rules to ignore the contents of the directory: _Testing/backstop_data/bitmaps_reference which is where your approved reference images are stored. If you want to work with a team using the BackstopJS regression tests with common approved reference images, either remove this line from the .gitignore to include these in your repo or come-up with another approved way of distributing changes to the "Approved" reference images (if you have image size concerns regarding your repo).

## Cleanup

Projects scaffolded with 'Tal's Node Grunt Site Template' can be spun up for testing quickly and can be torn down to cleanup disk space just as quickly. After playing around, testing and working for a while, simply run `grunt Nuke` to clear out all resources that are not part of the project's source code, including anything installed in node_modules. The next time you want to run and work with the project, you just run `npm install` and then `grunt Work` again.

## Grunt Task Help

Execute `grunt Help` or just `grunt` to get a list of available tasks, with those tasks intended to be called directly by users specifically called out.

## Additional Documentation

As part of the scaffolding, a 'Documentation' directory is added at the project root for... well, documentation. 

This directory will initially contain a copy of this ReadMe and any additional documentation about the template itself. Of course, the intent of this folder is that you can use it to also store documentation regarding your project. This includes any documentation generated by including JSDoc comments in your JavaScript source files.

As noted above, simply execute `grunt Docs` to have JSDoc generate HTML documentation files based on your JS file comments. The generated documentation will be written to an  "API\_Docs" folder inside the base "Documentation" folder.

# _Enabling Optional Build Tasks_

## Enabling OpenFL

To enable the grunt support for OpenFL, first make sure that you have both OpenFL and its dependency, Haxe, installed in your environment:

 - http://www.openfl.org/learn/docs/getting-started/ 

Once you have Haxe and OpenFL properly installed, open the gruntfile and go to the 'BasicBuild' task... uncomment the lines for:

 - 'Build_OpenFl',
 - 'sync:openFl_ToStaging',

**Note:** The tasks and elements needed to enable each of the optional build features have an associated line comment that starts with "// ENABLE -". Thus, you can search across the files in your project space to easily find all of these points.

This will include an OpenFL page build step and compile the included OpenFL test package.

## Enabling SVG to WebFont Building

To enable this feature, first make sure that you have the optional dependencies installed in your environment (FontForge at minimum), see the sections below on installing.

Once you have FontForge properly installed, open the gruntfile and go to the 'BasicBuild' task... uncomment the lines for:

 - 'webfont',
 - 'sync:fonts_built_ToStaging',

To get your built web font accessible in your pages:

Open 'AppSource/SOURCE_STYLES/_styleguides-sass_constants/_all.scss' and uncomment the line:

 - '@import "_styleguides-sass_constants/icon_fonts";'

**Note:** The tasks and elements needed to enable each of the optional build features have an associated line comment that starts with "// ENABLE -". Thus, you can search accross the files in your project space to easily find all of these points.

## Installing FontForge and/or TTFAutoHint

From the documentation for Grunt-Webfont ("SVG to webfont converter for Grunt"):

_"This plugin requires Grunt 0.4. Note that ttfautohint is optional, but your generated font will not be properly hinted if it’s not installed. And make sure you don’t use ttfautohint 0.97 because that version won’t work."_


### Installing Both on Mac

`brew install ttfautohint fontforge --with-python`

__Note__: This should result in both ttfautohint fontforge being available via the terminal. Verify by executing...

`ttfautohint -V`
and `fontforge -V`


### Installing Both on Linux

`sudo apt-get install fontforge ttfautohint`

__Note__: This should result in both ttfautohint fontforge being available via the terminal. Verify by executing...

`ttfautohint -V`
and `fontforge -V`


### Installing Both on Windows

[Download](http://www.freetype.org/ttfautohint/#download) and Install ttfautohint for Windows (optional).

[Download](http://fontforge.github.io/en-US/downloads/windows/) and install FontForge. Then, add the FontForge binary location to your PATH enviroment variable (e.g. "C:\Program Files (x86)\FontForgeBuilds\bin"). Verify by executing...

`ttfautohint -V`
and `fontforge -V`


# Markdown and ReadMe References

If you want to learn more about editing this ReadMe and creating good markdown-based readme files, refer the following sources:

* [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

* [Markdown-Guide](http://markdown-guide.readthedocs.io/en/latest/basics.html)

* [Visual Studio ReadMe Guidelines](https://www.visualstudio.com/en-us/docs/git/create-a-readme)
