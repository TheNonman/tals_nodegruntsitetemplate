package;

import openfl.display.Tile;
import openfl.geom.Point;

class Particle extends Tile
{
	public var vel_x:Float;
	public var vel_y:Float;
	public var acc_x:Float;
	public var acc_y:Float;

	public function new (  )
	{
		super();
	}
}
