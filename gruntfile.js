'use strict';

const BackstopJS = require('backstopjs');
const Chalk = require("chalk");
const CommandExists_Sync = require('command-exists').sync;
const LogFile = require("logfile-grunt");
//const Node_FS = require('fs');
//const Node_Path = require('path');
const Node_Exec = require('child_process').exec;

//
//
// # NOTES
// ================================================================================================
// ================================================================================================

// # GRUNT / NODE FILE MATCHING (GLOBBING) NOTES
// ----------------------------------------------
//
// Restrict matching to the content of one directory level down (i.e. single directory nesting):
//		'SomeRootDirectory/{,*/}*.js'
// Full Recursion, matching the content files in all subdirectory levels:
//		'SomeRootDirectory/**/*.js'
// Full Recursion, matching All content:
//		'SomeRootDirectory/**'
// Skip a directory during recursion:
//		'!**/SkipThisDirectoryName/**'



//
//
// Process Setup
// ================================================================================================
// ================================================================================================

const GruntProcessInfo = {
	PID: process.pid,
	Platform: process.platform
};

//const ProjectRoot_PATH = Node_Path.resolve(__dirname);
const ProjectProperties_PATH = `./BuildConfigs/ProjectProperties.js`;

const PrintEnvLoads_BOOL = false;


//
//
// # Get Available Environment Variables
// ================================================================================================
// ================================================================================================

const BuildEnvVars = require('./BuildConfigs/LoadBuildEnv.js')("GruntFile", PrintEnvLoads_BOOL);
console.log(`Build Environment Settings Load Complete...`);

const ServerEnvVars = require('./ServerConfigs/LoadServerEnv.js')("GruntFile", PrintEnvLoads_BOOL);
console.log(`Server Environment Settings Load Complete...`);

const ProjectProperties_OBJ = require(ProjectProperties_PATH);
console.log("Project Properties Load Complete...");



//
//
// Configure Process
// ================================================================================================
// ================================================================================================

const BackstopConfigPath = BuildEnvVars.BACKSTOP_CONFIG_PATH;
process.env.NODE_ENV = BuildEnvVars.NODE_ENV || 'development';

//console.log("process.env %o", process.env);


//
//
// # Definition of Grunt Task Handling
// ================================================================================================
// ================================================================================================

module.exports = function(grunt)
{
	//
	// # Load Sub-modules (i.e. Grunt Tasks)
	// (Simplify with "matchdep" or "load-grunt-tasks" or get fancy with "load-grunt-config")
	// --------------------------------------------------------------------------------------------

	// Load All Grunt Task Types (i.e. All Node Modules that Start with "grunt-")
	require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);

	// Load All Available Custom Grunt Tasks
	grunt.task.loadTasks('./BuildConfigs/Grunt/CustomTasks');


	//
	// # Load Project Structure Configuration
	// --------------------------------------------------------------------------------------------
	var NodePkg = grunt.file.readJSON('package.json');
	var JSHintConfigs = require('./BuildConfigs/Grunt/JSHintConfig.js');


	var LogFileOptions = {
		filePath: ProjectProperties_OBJ.tools.logs.build_logs_grunt + "/Grunt.log",
		clearLogFile: true,
		keepColors: false,
		textEncoding: "utf-8"
	};

	var GruntBuildOptions = {
								config: { src: "./BuildConfigs/Grunt/TaskConfigs/*.js" },
								NodeProjectPkg: NodePkg,
								BuildConfigs: ProjectProperties_OBJ,
								BuildEnvSettings: BuildEnvVars,
								ServerEnvSettings: ServerEnvVars,
								JSHintOptions: JSHintConfigs
							};

	// Load All Grunt TaskConfigs and Pass them the GruntBuildOptions along with a ref to them main Grunt process
	var TaskConfigs = require('load-grunt-configs')(grunt, GruntBuildOptions);


	//
	// # Helper Method Definitions
	// --------------------------------------------------------------------------------------------

	var RunCLI_Cmd = function(item, callback) {
		process.stdout.write('Running CLI Command, "' + item + '"...\n');
		var cmd = Node_Exec(item);
		cmd.stdout.on('data', function (data) {
			grunt.log.writeln(data);
		});
		cmd.stderr.on('data', function (data) {
			grunt.log.errorlns(data);
		});
		cmd.on('exit', function (code) {
			if (code !== 0) throw new Error('Running CLI Command, "' + item + '" failed');
			grunt.log.writeln('done\n');
			if (callback) {
				callback();
			}
		});
	};


	//
	// # Configure Grunt Tasks
	// --------------------------------------------------------------------------------------------

	grunt.initConfig(TaskConfigs);


	//
	// # Register (Executable) Grunt Task Aliases
	// --------------------------------------------------------------------------------------------

	// Define the DEFAULT task - executed when running grunt with no arguments
	grunt.registerTask('default', ['Help']);



	grunt.registerTask('TEST_Task_Fast', function() {
		grunt.log.write('Fast task finished.');
	});

	grunt.registerTask('TEST_Task_Block', function() {
		var ms = 1000;
		var start = +(new Date());
		while (new Date() - start < ms);
		grunt.log.write('Blocking finished.');
	});

	grunt.registerTask('TEST_Task_Fail', function() {
		var ms = 500;
		var start = +(new Date());
		while (new Date() - start < ms);
		grunt.log.error('Failure to be awesome!');
		throw new Error('Broken!');
	});

	grunt.registerTask('TEST_Parallel',
	function() {
		grunt.task.run([
			'StartProcessLog',
			'parallel'
		]);
	});

	grunt.registerTask('TEST_Concurrent',
	function() {
		grunt.task.run([
			'concurrent:test2:stream'
		]);
	});

	grunt.registerTask('BasicBuild', 'Basic One-time build (No Watches Set).',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'jshint:sourceDebug',

				'clean:full',
				//'Build_OpenFl', // ENABLE - OpenFl

				'WebFont_Check',
				'sass:production_site',
				'sass:production_3P',

				'sync:js_ToTemp',
				'sync:js3P_ToTemp',

				'sync:css_ToTemp',
				'sync:css_ToStaging',
				'sync:js_ToStaging',
				'sync:html_ToStaging',
				'shell:eleventy',
				'sync:html_BuiltTemplates_ToStaging',

				'copy:image_files_ToTemp',
				'copy:image_files_ToStaging',
				'sync:fonts_ToStaging',

				'WebFontSync_Check',
				//'sync:openFl_ToStaging', // ENABLE - OpenFl
			]);
		}
	);


	grunt.registerTask('Docs', 'Generate (Update) the API documentation with JSDoc.',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'jsdoc'
			]);
		}
	);


	grunt.registerTask('Help', 'Print project info and available grunt tasks.  < DEFAULT >',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'availabletasks'
			]);
		}
	);


	grunt.registerTask('Nuke', 'Clears All Build Content and Strips Down to Only Source (NPM Install will be Required to Work Again).',
		function() {
			grunt.task.run([
				'clean:strip_to_source'
			]);
		}
	);


	grunt.registerTask('Tests', 'Run Defined Tests.',
		function (  ) {
			// NOTE: the BackstopJS tests are executed targeting URLs on the Express server (which must start first).
			// This is required to be able to fully-resolve both absolute and relative paths in the server context.
			grunt.task.run([
				'express:dev',
				'Test_Backstop:test',
				'express:dev:stop'
			]);

			grunt.log.writeln("Tests COMPLETE");
		}
	);


	grunt.registerTask('TestsApproved', 'Confirm that Changes in Testing are Correct and Update Regression Test Assets.',
		function (  ) {
			grunt.task.run([
				'Test_Backstop:approve'
			]);
		}
	);


	grunt.registerTask('Test_Backstop', 'BackstopJS Visual Regression Testing Integration',
		function ( backstopCommand ) {
			// backstopCommand is either 'reference', 'test', or 'openReport'
			var done = this.async();
			BackstopJS(backstopCommand, { config: BackstopConfigPath }).then(function() {
				done(true);
			}).catch(function() {
				done(true);
			});
		}
	);


	grunt.registerTask('MultiWork', 'Starts the Development Build Cycle, Express Web Server, and Watches. Opens and updates in Chrome AND All Other Registered Browsers.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'express:dev',
				'browserSync_ByPlatform:all',
				'watch'
			]);
		}
	);


	grunt.registerTask('WebFont_Check', 'Executes building of icon fonts, but only if this is enabled in the build_settings.',
		function() {
			if (GruntBuildOptions.BuildEnvSettings.BUILD_WEB_FONTS.toLowerCase() !== "true") {
				grunt.log.writeln(Chalk.yellow('BUILD_WEB_FONTS is not enabled.'));
				return;
			}
			grunt.task.run([
				'webfont'
			]);
		}
	);


	grunt.registerTask('WebFontSync_Check', 'Executes syncing of built icon fonts to Staging, but only if this is enabled in the build_settings.',
		function() {
			if (GruntBuildOptions.BuildEnvSettings.BUILD_WEB_FONTS.toLowerCase() !== "true") {
				grunt.log.writeln(Chalk.yellow('BUILD_WEB_FONTS is not enabled.'));
				return;
			}
			grunt.task.run([
				'sync:fonts_built_ToStaging'
			]);
		}
	);


	grunt.registerTask('Work', 'Starts the Development Build Cycle, Express Web Server, and Watches. Opens and updates in Chrome only.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'express:dev',
				//'open', // Open task removed - This is handled by BrowserSync
				'browserSync_ByPlatform:one',
				'watch'
			]);
		}
	);


	// Default task(s).
	grunt.registerTask('StartProcessLog', 'Adds Process Info in the Log and Console',
		function() {

			new LogFile(grunt, LogFileOptions);

			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln(Chalk.green('Starting Grunt Tasking for...'));
			grunt.log.writeln(Chalk.green('	->	NPM Project Name: ' + grunt.config('NodeProjectPkg.name')));
			grunt.log.writeln(Chalk.green('	->	NPM Project Version: ' + grunt.config('NodeProjectPkg.version')));
			grunt.log.writeln(Chalk.green('	->	Using Grunt Version: ' + Chalk.blue(grunt.version)));

			var startTime = new Date();
			grunt.log.writeln(Chalk.green('	->	Process Started: ' + Chalk.blue(startTime)));
			grunt.log.writeln(Chalk.green('	->	Process PID: ' + Chalk.blue(GruntProcessInfo.PID)));
			grunt.log.writeln(Chalk.green('	->	Process Platform: ' + Chalk.blue(GruntProcessInfo.Platform)));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln("");
		}
	);



	grunt.registerTask('Build_OpenFl', 'Builds a Single Test OpenFL Project',
		function() {
			if (ProjectProperties_OBJ.openFL_project) {
				if ( grunt.file.exists( ProjectProperties_OBJ.openFL_project) ){
					grunt.log.writeln(Chalk.green('OpenFL Project File Exists: ' + ProjectProperties_OBJ.openFL_project));
					var commandName = "openfl";
					if (CommandExists_Sync(commandName)) {
						grunt.log.writeln(Chalk.green(commandName + '  IS  IN  FACT  INSTALLED !'));

						// Replace async library usage with native node async implementation
						var async = require('async');
						var done = this.async();

						async.series({
							openfl: function(callback){
								RunCLI_Cmd(commandName + ' build ' + ProjectProperties_OBJ.openFL_project + ' html5 -v', callback);
							}
						},
						function(err, results) {
							if (err) done(false);
							done();
						});
					}
					else {
						grunt.log.writeln(Chalk.red("ERROR: The CLI Command '" + commandName + "' is Not Available. Verify you have installed " + commandName));
					}
				}
				else {
					grunt.log.writeln(Chalk.green("No OpenFL Project File ('" + ProjectProperties_OBJ.openFL_project + "') Present. Skipping openfl build"));
				}
			}
			else {
				grunt.log.writeln(Chalk.green('No configuration for OpenFL Content'));
			}
		}
	);
};
