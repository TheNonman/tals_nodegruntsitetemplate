
const Find_PIDs = require('find-pid-from-port');
const KillPortProcess = require('kill-port');
const Node_FS = require('fs');
const Node_Path = require('path');

console.log(`Tal's Service and Process Utils`);
console.log(`---------------------------------------------------------`);

// Load Configured Service Settings
if (Node_FS.existsSync(Node_Path.join(__dirname, '../server_settings.env'))) {
	require('dotenv').config({ path: Node_Path.join(__dirname, '../server_settings.env')});
	console.log("Custom User Service ENV Loaded");
}
else if (Node_FS.existsSync(Node_Path.join(__dirname, '../server_settings_default.env'))){
	require('dotenv').config({ path: Node_Path.join(__dirname, '../server_settings_default.env')});
	console.log(`WARNING : Using Default Services Settings file ('.env'). To customize settings, duplicate the Defaults File ('server_settings_default.env') and rename it: server_settings.env`);
}
else {
	console.log(`ERROR :  No Valid Service Settings file ('.env') Available !!!`);
	return;
}

console.log(`---------------------------------------------------------`);

const ConfiguredServices = [
	{
		"ServiceName": "Express",
		"port" : process.env.EXPRESS_SERVER_PORT || 3300
	},
	{
		"ServiceName": "Koa",
		"port" : process.env.KOA_SERVER_PORT || 5000
	},
];

if (process.env.POSTGRES_DB_USE.toLocaleLowerCase() === "true") {
	ConfiguredServices.push(
		{
			"ServiceName": "Postgres SQL Connector",
			"port" : process.env.PG_HOST_PORT || UNDEFINED
		}
	);
};

if (process.env.REDIS_USE.toLocaleLowerCase() === "true") {
	ConfiguredServices.push(
		{
			"ServiceName": "Redis",
			"port" : process.env.REDIS_PORT || UNDEFINED
		}
	)
};




//
//
// Public Methods
// ===========================================================================
// ===========================================================================

async function CheckPorts (  )
{
	console.log(`CheckPorts -->`);

	for (const item of ConfiguredServices) {
		console.log(`--> Checking Port ${item.port} for ${item.ServiceName}`);
		const portPIDUse = await GetPortUse(parseInt(item.port));
		ReportServicePortUse(item.ServiceName, portPIDUse);
	};
};


async function KillPort ( targetPort, protocol="tcp" )
{
	console.log(`KillPort -->`);

	console.log(`--> Checking the target port ${targetPort}...`);
	let portPIDUse = await GetPortUse(parseInt(targetPort));
	if (portPIDUse === "FREE") {
		console.log(`--> There doesn't seem to be a process using port ${targetPort}, ...`);
		console.log(`--> This will probably result in an error, FYI...`);
	}
	else {
		console.log(`--> Working on Killing the Process, this may take a minute...`);
	}

	await KillPortProcess(targetPort, protocol)

	console.log(`--> Checking Port ${targetPort} After Kill...`);
	portPIDUse = await GetPortUse(parseInt(targetPort));

	ReportServicePortUse("Requested", portPIDUse);
};


async function KillAllPorts (  )
{
	console.log(`KillAllPorts -->`);
	console.log(`--> Checking All Configured Ports...`);
	console.log(`--> Keep in mind any configured ports that aren't in use by a running process`);
	console.log(`--> will throw an error when we attempt to kill it.`);
	console.log(`---------------------------------------------------------`);

	await CheckPorts();

	console.log(`---------------------------------------------------------`);
	console.log(`--> Working on killing configured port processes, this may take a minute...`);

	for (const item of ConfiguredServices) {
		console.log(`--> Killing any process using Port ${item.port} for ${item.ServiceName}`);
		try {
			await KillPort(parseInt(item.port));
		}
		catch (error) {
			console.log(`--> Killing process for Port ${item.port} failed...`);
			console.log(`--> Error: ${error}`);
		}
	};
	console.log(`--> All configured ports should by clear... rechecking...`);
	console.log(`---------------------------------------------------------`);

	await CheckPorts();
};



//
//
// Private Methods
// ===========================================================================
// ===========================================================================

const GetPortUse = async (targetPort) => {
	try {
		const targetPortPIDs = await Find_PIDs(targetPort);
		return targetPortPIDs;
	}
	catch (error) {
		if(!error.toString().includes("Couldn't find a process with port")) {
			console.log("WARNING :: Unexpected Error in GetPortUse()...");
			console.log("--> Error: %o", error);
			return "UNKNOWN";
		}
		return "FREE";
	}
};


function ReportServicePortUse ( serviceName, portUseInfo )
{
	if (portUseInfo === "FREE") {
		console.log(`--> ${serviceName} Port is FREE`);
	}
	else if (portUseInfo === "UNKNOWN") {
		console.log(`--> COULD NOT VERIFY FREE STATE of ${serviceName} Port... SEE ERROR ABOVE !!!`);
	}
	else {
		console.log(`--> ${serviceName} Port in use by PID(s) ${portUseInfo.all}`);

		if (portUseInfo.tcp && portUseInfo.tcp.length > 0)
			console.log(`--> -->  ${serviceName} Port TCP use PID ${portUseInfo.tcp}`);

		if (portUseInfo.udp && portUseInfo.udp.length > 0)
			console.log(`--> -->  ${serviceName} Port UDP use PID ${portUseInfo.udp}`);
	}
};




module.exports = {
	CheckPorts: CheckPorts,
	KillPort: KillPort,
	KillAllPorts: KillAllPorts
}
