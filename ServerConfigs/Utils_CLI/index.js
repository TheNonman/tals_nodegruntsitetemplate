#!/usr/bin/env node

const program = require('commander');
const colors = require('colors');

const Utils = require('./main_utils');

program
	.name('Utils_CLI')
	.description('Utility commands for working with ports and processes.');

program.command('CheckPorts')
	.description('Check current system use of relevant ports.')

	.action(() => {
		Utils.CheckPorts();
	});

program.command('KillPort')
	.description('Kill process currently using the target port.')
	.argument('<port>', 'Port number to kill')
	.argument('[protocol]', 'Protocol to match with the Port to Kill. "tcp" or "udp"')
	.action((port, protocol) => {
		Utils.KillPort(port, protocol);
	});


program.command('KillAllPorts')
	.description('Kill any and all process currently using ports configured for use in this project.')
	.action(() => {
		Utils.KillAllPorts()
});



program.showHelpAfterError()
	.parse(process.argv);
