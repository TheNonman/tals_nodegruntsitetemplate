
// Server Base & Middleware
const Koa = require('koa');
const cors = require('kcors');
const KoaStaticFiles = require('koa-static');
const Node_FS = require('fs');
const Node_Path = require('path');



//
//
// Pre-Setup Configuration
// ===========================================================================
// ===========================================================================

const log = require('./logger');
log.info(`Koa Server Starting...`);



if (Node_FS.existsSync(Node_Path.join(__dirname, '../server_settings.env'))) {
	require('dotenv').config({ path: Node_Path.join(__dirname, '../server_settings.env')});
	log.info("Koa... Custom User Server ENV Loaded");
}
else if (Node_FS.existsSync(Node_Path.join(__dirname, '../server_settings_default.env'))){
	require('dotenv').config({ path: Node_Path.join(__dirname, '../server_settings_default.env')});
	log.warn(`Koa... WARNING : Using Default Server Settings file ('.env'). To customize settings, duplicate the Defaults File ('server_settings_default.env') and rename it: server_settings.env`);
}
else {
	log.error(`Koa... WARNING :  No Valid Build Settings file ('.env') Available !!!`);
}

// Connected Server Components
const api_router = require('./api-router');



//
//
// Setup Koa App
// ===========================================================================
// ===========================================================================

const app = new Koa();
const port = process.env.KOA_SERVER_PORT || 5000;
const staticFileServiceOptions = {
	// Included Options are Not All
	index: 'index.html', 	// autoloaded file name
	defer: true,			// Uses/checks defined routes first before file resolution
}

// Apply CORS config
const origin = process.env.KOA_CORS_ORIGIN + ":" + process.env.KOA_SERVER_PORT | '*';
app.use(cors({ origin }));

if (process.env.KOA_STATIC_FILE_DIR && process.env.KOA_STATIC_FILE_DIR.toLowerCase() != "none") {
	app.use(KoaStaticFiles(process.env.KOA_STATIC_FILE_DIR, staticFileServiceOptions))
	log.warn(`[Koa]  Static File Service Enabled from ${process.env.KOA_STATIC_FILE_DIR}`);
}
else {
	log.warn("[Koa]  Static File Service DISABLED");
}


// Log all requests
app.use(async (ctx, next) => {
	const start = Date.now();
	let routerMsg = '';

	await next(); // This will pause this function until the endpoint handler has resolved

	const responseTime = Date.now() - start;
	if (routerMsg.length > 0) {
		log.error(`${ctx.method} ${ctx.status} ${ctx.url} - ${routerMsg} - ${responseTime} ms`);
		return;
	}
	log.info(`${ctx.method} ${ctx.status} ${ctx.url} - ${responseTime} ms`);
});

// Error Handler - All uncaught exceptions will percolate up to here
app.use(async (ctx, next) => {
	try {
		await next();
	}
	catch (err) {
		ctx.status = err.status || 500;
		ctx.body = err.message;
		log.error(`Request Error ${ctx.url} - ${err.message}`);
	}
});

// Mount routes
app.use(api_router.routes(), api_router.allowedMethods());

// Start the app
app.listen(port, () => {
	log.info(`Koa Server STARTED :: Listening on PORT ${port}`)
	log.info(`Koa Server STARTED :: PID ${process.pid}`)
	log.info(`Koa Server STARTED :: Platform ${process.platform}`)
});
