

// Component Base & Middleware
const Router = require('koa-router');

// Connected Components
const cache = require('./redis-cache');
const PostgresDB = require('./postgres-db');
const log = require('./logger');



//
//
// Setup Router
// ===========================================================================
// ===========================================================================

const router = new Router();
let PostgresService = null;


//
//
// Router Service Configuration By Environment
// ===========================================================================
// ===========================================================================

let Is_DBAvailable_BOOL = false;
// Try to Configure and Use Postgres DB (Only if User wants It)
if (process.env.POSTGRES_DB_USE === "true") {
	PostgresService = PostgresDB.getInstance();
	let currentDBStatus = PostgresService.getDBStatus();

	switch(currentDBStatus) {
		case PostgresDB.CONNECTION_STATE_STARTING :
			log.info(`[api-router]  Waiting for PostgresDB_StatusChange`);
			PostgresService.on("PostgresDB_StatusChange", ( newStatus ) => {
				log.info(`[api-router]  on PostgresDB_StatusChange: new Status: ${newStatus}`);
				Is_DBAvailable_BOOL = (PostgresService.getDBStatus() === PostgresDB.CONNECTION_STATE_AVAILABLE);
				log.info(`[api-router]  PostgresDB_StatusChange: Is_DBAvailable_BOOL: ${Is_DBAvailable_BOOL}`);
				if (!Is_DBAvailable_BOOL) {
					log.error(`[api-router]  Postgres DB FAILED to Create a Usable Connection !!!`);
					log.error(`[api-router]  --> Is the server running?`);
				}
			});
			break;
		case PostgresDB.CONNECTION_STATE_AVAILABLE :
			Is_DBAvailable_BOOL = true;
			break;
		case PostgresDB.CONNECTION_STATE_UNAVAILABLE :
			log.error(`[api-router]  Postgres DB FAILED to Create a Usable Connection !!!`);
			log.error(`[api-router]  --> Is the server running?`);
			break;
		default :
			log.error(`[api-router]  INVALID Starting Postgres DB Status Encountered: "${currentDBStatus}"`);
			break;
	}
}
else {
	log.warn("[api-router]  Postgres DB Use DISABLED");
}

if (cache.IsRedisAvailable()) {
	log.info("[api-router]  Redis Cache Use ENABLED");

	// Check Cache... before continuing to any endpoint handlers (i.e. prefer cache)
	router.use(cache.checkResponseCache);

	// Store Responses... in cache once handlers have finished
	router.use(cache.addResponseToCache);
}
else {
	log.warn("[api-router]  Redis Cache Use DISABLED");
}



//
//
// Routes
// ===========================================================================
// ===========================================================================

// Hello World Test Endpoint
router.get('/hello', async ctx => {
	ctx.body = 'Hello World'
});



// Database Query Routes
// Make sure to validate by checking Is_DBAvailable_BOOL
// ===========================================================================
// ===========================================================================

router.get('/db_time', async ctx => {
	if(!Is_DBAvailable_BOOL) {
		log.warn("[api-router]  DB Query Route Accessed with DB Access Unavailable !!!");
		return;
	}

	const result = await PostgresService.queryTime();
	ctx.body = result;
});




module.exports = router;
