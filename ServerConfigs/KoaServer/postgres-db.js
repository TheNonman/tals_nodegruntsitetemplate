
// Component Base & Middleware
const postgres = require('pg');
const { EventEmitter } = require('events');

// Connected Components
const log = require('./logger');


class PostgresDB extends EventEmitter
{
	static CONNECTION_STATE_STARTING = "Starting";
	static CONNECTION_STATE_AVAILABLE = "Available";
	static CONNECTION_STATE_UNAVAILABLE = "Unavailable";

	static EVENT_STATUS_CHANGE = "PostgresDB_StatusChange";


	constructor ( )
	{
		super();

		this.client = new postgres.Client({
			host: process.env.PG_HOST,
			port: process.env.PG_HOST_PORT,
			user: process.env.PG_USER,
			database: process.env.PG_DB,
			ssl: (process.env.PG_SSL === "true")
		});
		this.ConnectionState = PostgresDB.CONNECTION_STATE_STARTING;

		this.Init();
	};

	static getInstance()
	{
		if (!this.instance) {
			this.instance = new PostgresDB();
		}

		return this.instance;
	}

	//
	//
	// Setup DB Connection
	// ===========================================================================
	// ===========================================================================
	Init (  )
	{
		log.info(`[postgres-db]  Init()`);

		// Attempt Startup Connection and Configure Component Status Based on Result
		this.client.connect()
		.then(() => {
			log.info(`[postgres-db]  Connected To ${this.client.database} at ${this.client.host}:${this.client.port}`);
			this.ConnectionState = PostgresDB.CONNECTION_STATE_AVAILABLE;
			this.emit(PostgresDB.EVENT_STATUS_CHANGE, this.ConnectionState);
		})
		.catch((error) => {
			log.error(`[postgres-db]  Postgres DB Connection FAILED`);
			log.error(`[postgres-db]  -> Postgres Connect Response: ${error}`);
			this.ConnectionState = PostgresDB.CONNECTION_STATE_UNAVAILABLE;
			this.emit(PostgresDB.EVENT_STATUS_CHANGE, this.ConnectionState);
		});
	};

	getDBStatus (  )
	{
		return this.ConnectionState;
	};

	/** Query Server Time */
	async queryTime (  )
	{
		const result = await this.client.query('SELECT NOW() as now');
		return result.rows[0];
	};
};




//
//
// Export Class
// ===========================================================================
// ===========================================================================

module.exports = PostgresDB;
