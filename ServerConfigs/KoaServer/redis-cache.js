
// Connected Components
const log = require('./logger');



//
//
// Default Exports and Config
// ===========================================================================
// ===========================================================================

// Base User Environment Config
let IsRedisUsed_BOOL = (process.env.REDIS_USE.toLocaleLowerCase() === "true");
let Redis_Connection = null;

if (IsRedisUsed_BOOL) {
	try {
		const Redis = require('ioredis');
		Redis_Connection = new Redis(process.env.REDIS_PORT, process.env.REDIS_HOST);

		Redis_Connection.on("connect", (theEvent) => {
			log.info(`[redis-cache]  Redis CONNECT`,);
		});
	}
	catch (theError) {
		log.error('[redis-cache]  FAILED LOADING (i.e. require) ioredis');
		log.error('-> install ioredis or disable REDIS_USE in your .env');
		IsRedisUsed_BOOL = false;
	}
};

module.exports = {
	IsRedisAvailable (  )
	{
		return IsRedisUsed_BOOL;
	}
};

if (!IsRedisUsed_BOOL)				return;



//
//
// Extend Component When Configured and Middleware Installed/Available
// ===========================================================================
// ===========================================================================

async function checkResponseCache (ctx, next)
{
	const cachedResponse = await Redis_Connection.get(ctx.path)
	if (cachedResponse) { // If cache hit
		ctx.body = JSON.parse(cachedResponse) // return the cached response
	} else {
		await next() // only continue if result not in cache
	}
};


/** Koa middleware function to insert response into cache */
async function addResponseToCache ( ctx, next )
{
	await next() // Wait until other handlers have finished
	if (ctx.body && ctx.status === 200) { // If request was successful
		// Cache the response
		await Redis_Connection.set(ctx.path, JSON.stringify(ctx.body))
	}
};


//
//
// Add Public Methods to Exports
// ===========================================================================
// ===========================================================================

module.exports.checkResponseCache = checkResponseCache;
module.exports.addResponseToCache = addResponseToCache;
