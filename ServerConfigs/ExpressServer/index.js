
// Server Base & Middleware
const Express = require('express');
const Node_FS = require('fs');
const Node_Path = require('path');



//
//
// Pre-Setup Configuration
// ===========================================================================
// ===========================================================================

const Logger = require('./logger');
Logger.info(`Express Server Starting...`);

if (Node_FS.existsSync(Node_Path.join(__dirname, '../server_settings.env'))) {
	require('dotenv').config({ path: Node_Path.join(__dirname, '../server_settings.env')});
	Logger.info("Express... Custom User Server ENV Loaded");
}
else if (Node_FS.existsSync(Node_Path.join(__dirname, '../server_settings_default.env'))){
	require('dotenv').config({ path: Node_Path.join(__dirname, '../server_settings_default.env')});
	Logger.warn(`Express... WARNING : Using Default Server Settings file ('.env'). To customize settings, duplicate the Defaults File ('server_settings_default.env') and rename it: server_settings.env`);
}
else {
	Logger.error(`Express... WARNING :  No Valid Build Settings file ('.env') Available !!!`);
}

const port = process.env.EXPRESS_SERVER_PORT || 3300;




//
//
// Setup Express App
// ===========================================================================
// ===========================================================================

const app = Express();
app.set('port', port); // Define the port to run on
const staticFileServiceOptions = {
	// Included Options are Not All
	index: ['index.html' , 'index.htm'], 	// autoloaded file name
};

app.use((req, res, next) => {
	Logger.info(`${req.method} ${req.originalUrl} - ${Date.now()}`);
	next();
});

app.use(require('./router_base').Base_Routes);

if (process.env.EXPRESS_STATIC_FILE_DIR && process.env.EXPRESS_STATIC_FILE_DIR.toLowerCase() != "none") {
	app.use(Express.static(Node_Path.join(__dirname, "../../", process.env.EXPRESS_STATIC_FILE_DIR), staticFileServiceOptions));
	Logger.info(`Express...  Static File Service Enabled from ${process.env.KOA_STATIC_FILE_DIR}`);
}
else {
	Logger.warn("Express...  Static File Service DISABLED");
};

// Listen for requests
var server = app.listen(app.get('port'), function() {
	Logger.info(`Express Server STARTED :: Listening on PORT: ${port}`);
});
