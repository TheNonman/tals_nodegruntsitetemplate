
const winston = require('winston')
const Node_Path = require('path')

// Configure custom app-wide logger
module.exports = winston.createLogger({
	transports: [
		new (winston.transports.Console)(),
		new (winston.transports.File)({
			name: 'info-file',
			filename: Node_Path.resolve(__dirname, '../../__BuildLogs/ExpressServerLogs/info.log'),
			level: 'info'
		}),
		new (winston.transports.File)({
			name: 'error-file',
			filename: Node_Path.resolve(__dirname, '../../__BuildLogs/ExpressServerLogs/error.log'),
			level: 'error'
		})
	]
})
