
// Server Base & Middleware
const Express = require('express');
const Base_Routes = Express.Router();

// Connected Components
const Logger = require('./logger');
const Log_ID = "[router_base]";

Base_Routes.use(function (req, res, next)
{
	// .. some logic here .. like any other middleware
	next()
})

Base_Routes.get('/hello', function (req, res)
{
	Logger.info(`${Log_ID}  Hello Dev`);
	res.send('Hello Dev');
});

// =============================================================================


module.exports.Base_Routes = Base_Routes;
